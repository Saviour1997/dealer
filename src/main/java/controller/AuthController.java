package controller;

import entity.User;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import security.jwt.JwtProvider;
import service.UserService;

@RestController
@RequiredArgsConstructor
public class AuthController {

    private JwtProvider jwtProvider;

    private AuthenticationManager authenticationManager;

    private UserService userService;

    public ResponseEntity<User> responseEntity (@Valid @RequestBody Si)
}
