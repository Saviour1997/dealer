package controller;


import entity.Game;
import org.springframework.web.bind.annotation.*;
import service.GameService;

import java.util.List;

@RestController
public class GameController {

    private final GameService gameService;

    public GameController(GameService gameService) {
        this.gameService = gameService;
    }

    @RequestMapping(value = "/games", method = RequestMethod.POST)
    public Game addGame(@RequestBody Game newGame) {

        return gameService.addGame(newGame);
    }

    @RequestMapping(value = "/games/{id}", method = RequestMethod.PUT)
    public Game editGame(@PathVariable long id, @RequestBody Game newGame) {

        return gameService.editGame(id, newGame);
    }

    @RequestMapping(value = "/games", method = RequestMethod.GET)
    public List<Game> getGames() {

        return gameService.getAll();
    }
}
