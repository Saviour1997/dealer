package controller;

import entity.Post;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import service.PostService;

import java.util.List;

@RestController
public class PostController {

    private final PostService postService;

    public PostController(PostService postService) {
        this.postService = postService;
    }

    @RequestMapping(value = "/articles", method = RequestMethod.POST)
    public Post addPost(@RequestBody Post newPost) {

        return postService.addPost(newPost);
    }

    @RequestMapping(value = "/my", method = RequestMethod.GET)
    public List<Post> getGames() {

        return postService.getAll();
    }
}
