package entity.enums;

public enum Role {
    ADMINISTRATOR, TRADER, ANONYMOUS
}
