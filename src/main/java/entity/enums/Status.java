package entity.enums;

public enum Status {
    ACTIVE,
    INACTIVE,
    DELETED
}
