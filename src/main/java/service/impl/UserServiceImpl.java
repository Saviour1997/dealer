package service.impl;

import entity.User;
import entity.enums.Role;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import redis.clients.jedis.Jedis;
import repository.UserRepository;
import security.jwt.JwtProvider;
import service.MailService;
import service.UserService;


import java.util.Date;
import java.util.List;

@Service
@RequiredArgsConstructor
@Slf4j
public class UserServiceImpl implements UserService {

    private Jedis jedis;
    private UserRepository userRepository;
    private MailService mailService;
    private JwtProvider jwtUtils;


    @Override
    public User addUser(User user) {
        return userRepository.saveAndFlush(user);
    }

    @Override
    public User getUserById(long id) {
        return userRepository.findById(id).orElse(new User());
    }

    @Override
    public User getUserByEmail(String email) {
        User user = userRepository.findByEmail(email);
        if (user == null)
            throw new UsernameNotFoundException(email);
        return userRepository.findByEmail(email);
    }

    @Override
    public void delete(long id) {
        userRepository.deleteById(id);
    }

    @Override
    public User editUser(User user) {
        return userRepository.saveAndFlush(user);
    }

    @Override
    public List<User> getAll() {
        return userRepository.findAll();
    }

    @Override
    public String register(User user, String path) {
        String token = jwtUtils.generateToken(user.getEmail());
        jedis.set(token, user.getEmail());

        mailService.sendEmail(user.getEmail(), "REGISTRATION", "http://localhost:8080" + path + "/confirm?token=" + token);

        user.setCreationDate(new Date());
        user.setRole(Role.TRADER);
        userRepository.save(user);

        return token;
    }

    @Override
    public void resetPassword(String email, String password) {
        String token = jwtUtils.generateToken(email);
        jedis.set(token, email);
        mailService.sendEmail(email, "RESET PASSWORD", "http://localhost:8080/auth/reset?token=" + token + "&password=" + password);
    }


    @Override
    public void confirm(String token) {
        if(jedis.exists(token) && jwtUtils.validateToken(token)){
            String email = jedis.get(token);
            User userByEmail = userRepository.findByEmail(email);
            System.out.println(userByEmail);
            userByEmail.setEnabled(true);
            userRepository.save(userByEmail);
            log.info("User with email " + email + " is confirmed");
        }else {
            log.info("There is not such a token");
        }
    }


}
